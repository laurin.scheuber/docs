= Begrenzte Schleifen und Verwendung von Objekten
:sectnums:
:appendix-caption: Anhang

== Die `for`-Schleife im Detail

=== Vergleichsoperatoren

Der Abbruch einer Schleife wird immer durch einen _Test_ gemacht, der überprüft, ob der Schleifen-Body noch ein weiteres mal ausgeführt werden soll. Bei der `for`-Schleife steht der Test in der Mitte des Schleifenkopfs:

[source,java]
----
for (int i = 1; /* Test: */ i <= 10; i = i + 1) {
    // Body
}
----

Ein einfacher Test wie dieser vergleicht zwei Ausdrücke, in diesem Fall den Ausdruck `i` mit dem Ausdruck `10`. Statt `i` und `10` könnten beliebige arithmetische Ausdrücke verglichen werden.

Interessanterweise ist der Test selber auch wieder ein Ausdruck. Dieser ergibt als Resultat keine Zahl, sondern einen Wahr-/Falsch-Wert vom Typ `boolean`, also `true` oder `false`. Das folgende Stück Code erzeugt z. B. die Ausgabe `true` auf der Konsole:

[source,java]
----
int i = 5;
System.out.println(i <= 8);
----

Es gibt sechs Operatoren, um Vergleiche zu machen: `<`, `>`, `+<=+`, `>=`, `==` und `!=`. Die letzten beiden bedeuten «gleich» und «ungleich». Achten Sie darauf, dass Sie den Gleichheitsoperator `==` nicht mit dem Zuweisungsoperator `=` verwechseln!

Die Vergleichsoperatoren haben eine niedrigere Präzedenz als die arithmetischen Operatoren. Dadurch kann man Ausdrücke wie `i / 2 < x` schreiben, und sie verhalten sich so, wie man das aus der Mathematik kennt. Die neue Präzedenztabelle sieht so aus:

[%autowidth]
|===
|Beschreibung                        |Operatoren |Präzedenz

|unäre Operatoren (z. B. `-4`) |`+`, `-`               |höchste
|multiplikative Operatoren         |`*`, `/`, `%`          |
|additive (binäre) Operatoren      |`+`, `-`               |
|Vergleichsoperatoren              |`<`, `>`, `+<=+`, `>=` |
|Gleichheitsoperatoren             |`==`, `!=`             |niedrigste
|===

=== Schleifenupdate und abgekürzte Schreibweisen

Das _Update_ einer Schleife, der letzte Teil im Schleifenkopf, ist im Prinzip eine normale Zuweisung, welche die Schleifenvariable verändert. Typischerweise wird der aktuelle Wert in der Variable ausgelesen, verändert und wieder in dieselbe Variable zurückgeschrieben. Dabei können auch andere Schritte als +1 gemacht werden, zum Beispiel:

[source,java]
----
for (int i = 0; i <= 10; i = i + 2) {
    // Body
}
----

Da solche Zuweisungen, die eine Variable lesen und gleich wieder schreiben, häufig in Programmen gebraucht werden, gibt es dafür angekürzte Schreibweisen:

[%autowidth]
|===
|Lange Form  |Kurzform

|`a = a + 2` |`a += 2`
|`a = a - 2` |`a -= 2`
|`a = a * 2` |`a *= 2`
|`a = a / 2` |`a /= 2`
|`a = a % 2` |`a %= 2`
|===

In Java (und vielen anderen Sprachen) gibt es sogar eine noch kürzere Schreibweise, spezifisch das Erhöhen bzw. das Verringern einer Zahl um 1 (Inkrement, Dekrement):

[%autowidth]
|===
|Lange Form  |Kurzform |Superkurzform

|`a = a + 1` |`a += 1` |`a++`
|`a = a - 1` |`a -= 1` |`a--`
|===

Ab jetzt schreiben wir Schleifen entsprechend so:

[source,java]
----
for (int i = 1; i <= 8; i++) {
    forward(50);
    right(45);
}
----

=== Verschachtelte Schleifen

In einer Schleife können beliebige Anweisungen stehen. So auch noch weitere Schleifen.

*Beispiel:*

[source,java]
----
for (int x = 90; x > 0; x -= 2) {
    for (int y = 2; y < 22; y *= 4){
        System.out.prtinln(x + " - " + y);
    }
}
----

Wichtig ist hier, dass alle Zeilen richtig eingerückt sind, sodass auch gut sichtbar ist, was wiederholt wird. In IntelliJ kann der Code auch automatisch formatiert werden, mit _Code -> Reformat Code_.

== `String`-Objekte

=== Strings als Objekte
Letzte Woche haben wir «primitive» Daten kennengelernt: einzelne Zahlen, Zeichen oder Wahr-/Falsch-Werte. Entsprechend hat jede Variable auch nur einen einzelnen Wert gespeichert.

Oft gehören allerdings mehrere Daten zusammen, z. B. die Informationen zu einem Bankkonto: Kontonummer, Name des Inhabers und der Kontostand. Zu den Daten kommt meist noch _Verhalten_ hinzu: Bei einem Konto kann man beispielweise Geld abheben oder einzahlen. In objektorientierten Sprachen wie Java packt man verschiedene Daten und Verhalten zusammen in sogenannte _Objekte_.

Wie primitive Werte haben auch Objekte in Java immer einen Typ. Dazu gehören auch Strings! Wenn man mittels Anführungszeichen einen String im Programm angibt, erzeugt man tatsächlich ein Objekt vom Typ `String`. Dieses Objekt kann man wie andere Daten in eine Variable mit dem entsprechenden Typ speichern:

[source,java]
----
String s1 = "Hello";
String s2 = "there";
String zusammen = s1 + " " + s2;
System.out.println(zusammen);
----

Dass man die `+`-Operation mit String-Objekten verwenden kann, ist übrigens eine Ausnahme in Java; Operatoren funktionieren ansonsten nur mit primitiven Werten. Operationen im Zusammenhang mit Objekten werden hingegen üblicherweise durch einen _Methodenaufruf_ ausgeführt. Ein solcher Aufruf verwendet die sogenannte «_Dot_-Notation», also einen Punkt:

[source,java]
----
int laenge = zusammen.length();
----

Der Aufruf oben ruft die Länge des Strings ab, der gerade in der zuvor in der Variable `zusammen` gespeichert wurde.

Die einzelnen Zeichen (``char``s) eines Strings können ebenfalls durch Methodenaufrufe abgefragt werden, mit der Methode `charAt`. Dazu muss die Position, auch genannt _Index_, des Zeichens innerhalb des Strings bekannt sein. Eventuell gewöhnungsbedürftig ist dabei, dass das erste Zeichen den Index `0` hat (und nicht etwa `1`), das zweite den Index `1`, usw. Das letzte Zeichen von einem String mit Länge 7 hat z. B. auch nicht Index 7, sondern 6. Das folgende Codestücke gibt entsprechend die Buchstaben `B` und `e` aus:

[source,java]
----
String text = "Bye bye";
char erstes = text.charAt(0);
System.out.println(erstes);
char letztes = text.charAt(6);
System.out.println(letztes);
----


=== Strings und Schleifen

Um Strings zeichenweise zu verarbeiten, braucht man oft Schleifen. Folgendes Beispiel liest jedes Zeichen eines Strings aus und gibt es einzel auf einer Zeile in der Konsole aus:

[source,java]
----
String text = "Guten Morgen";
for (int i = 0; i < text.length(); i++) {
    char zeichen = text.charAt(i);
    System.out.println(zeichen);
}
----

Beachten Sie, dass die Schleife als Abbruchbedingung nicht einfach die Zahl 12, die Länge des Strings, verwendet, sondern den Ausdruck `i < text.length()`. Dadurch funktioniert dieser Code auch noch, wenn man `text` auf einen längeren oder kürzeren String ändert.


=== Escape-Sequenzen

Es kann vorkommen, dass man einen String erzeugen möchte, der spezielle Zeichen enthält, z. B. das `"` oder einen Zeilenumbruch. Diese Zeichen können nicht ohne Weiteres in einem String angegeben werden (ein String geht ja bis zum zweiten `"` und darf nicht über mehrere Zeilen gehen). Hier helfen _Escape-Sequenzen_, welche mit einem `\` beginnen und von Java speziell interpretiert werden:

[%autowidth]
|===
|Escape-Sequence | wird interpretiert als

|`\t` |Tab-Zeichen (Tabulator)
|`\n` |Zeilenumbruch
|`\"` |Anführungszeichens
|`\\` |einzelner Backslash
|===

Der erste `\` wird nur zur Identifikation der Escape-Sequenz verwendet und wird nicht Teil des erzeugten String-Objekts. Dies kann man mit folgendem Codestücke überprüfen:

[source,java]
----
String text = "\"A\"\nO";
System.out.println(text);
System.out.println(text.length());
----

Die Ausgabe lautet:

    "A"
    O
    5


=== Teil-Strings extrahieren und wichtigste String-Methoden

Eine weitere wichtige Methode zum Verarbeiten von Strings ist `substring`, welche einen Teil des String zurückgibt. Folgendes Codestücke extrahiert den Teilstring, der bei Index 1 beginnt und bis _vor_ (!) Index 4 geht:

[source,java]
----
String s = "Morgen!";
String teil = s.substring(1, 4);
System.out.println(teil);
----

Die Ausgabe lautet entsprechend:

    org

Folgende Tabelle fasst die wichtigsten String-Methoden zusammen:

[%autowidth]
|===
|Methode                  |Beschreibung                 |Beispiel (`s` ist `"Hello"`)

|`length()`               |Anzahl Zeichen im String     |`s.length` gibt `5`
|`charAt(index)`          |Zeichen an bestimmter Stelle |`s.chatAt(1)` gibt `'e'`
|`substring(start, ende)` |alle Zeichen von `start`-Index bis direkt vor `ende`-Index |`s.substring(1, 3)` gibt `"el"`
|`startsWith(str)`        |ob der String mit `str` beginnt |`s.startWith("a")` gibt `false`
|`endsWith(str)`          |ob der String mit `str` endet   |`s.endsWith("llo") gibt `true`
|`indexOf(str)`           |Stelle, wo `str` zum ersten Mal vorkommt (`-1` falls nirgends) |`s.indexOf("l")` gibt `2`
|`replace(s1, s2)`        |ersetzt alle Vorkommnisse von `s1` mit `s2` |`s.replace("l", "yy")` gibt `"Heyyyyo"`
|`toLowercase()`          |neuer String mit lauter Kleinbuchstaben |`s.toLowerCase()` gibt `"hello"`
|`toUpperCase()`          |neuer String mit lauter Grossbuchstaben |`s.toUpperCase()` gibt `"HELLO"`
|===


=== String-Objekte sind unveränderbar

Obwohl Methoden wie `toLowerCase()` den Eindruck erwecken, dass man den Text in einem String-Objekt verändern kann, ist das nicht der Fall: String-Objekte sind _unveränderbar_. Tatsächlich erzeugt folgendes Codestücke auch nicht die erwartete Ausgabe:

[source,java]
----
String s = "Hallo Java";
s.toLowerCase();
System.out.println(s);
----

Wenn man diesen Code ausführt, kommt `Hallo Java` raus; es scheint als würde `toLowerCase()` gar nichts machen! Allerdings wurde die Methode einfach falsch verwendet: Sie produziert nämlich _ein neues String-Objekt_, das man wieder in eine Variable speichern (oder sonst irgendwie verwenden) muss:

[source,java]
----
String s = "Hallo Java";
s = s.toLowerCase();
System.out.println(s);
----

Jetzt wird der erwartete Text `hallo java` ausgegeben. Wenn Sie hier Verständnisschwierigkeiten haben, nehmen Sie zum Vergleich folgenden Code, der auch nicht wie gewünscht funktioniert:

[source,java]
----
int i = 1;
i + 1;
System.out.println(i);
----

Stattdessen muss man auch hier das Resultat des Ausdrucks `i + 1` wieder in die Variable zurückspeichern, um einen Effekt zu erzielen: `i = i + 1;`. (Das Codestück oben kann man übrigens gar nicht ausführen, da der Compiler schon merkt, dass die zweite Zeile sinnlos ist. Bei `s.toLowerCase()` ist das nicht der Fall, aber einige IDEs zeigen immerhin eine Warnung an.)


=== `char` vs. `int`

Ein String besteht aus einer Reihe von `char`-Werten. Intern speichert der Computer diese als Zahlen, ähnlich zu ``int``s. Eine _Zeichencodierung_ gibt an, welche Zeichen auf welche Zahlen abgebildet werden. Das kann man sich als Programmierer zunutze machen, um ``char``s als ``int``s zu behandeln und mit ihnen zu «rechnen».

Java erlaubt eine automatische Konvertierung von `char` nach `int`. Das folgende Codestück gibt z. B. den Buchstaben `'a'` als Zahl aus:

[source,java]
----
int c = 'a';
System.out.print(c);
----

Die Ausgabe lautet `97`. Daraus lässt sich schliessen, dass das Zeichen `a` in der von Java verwendeten Zeichenkodierung der Zahl 97 entspricht. Auch arithmetische und Vergleichsoperatoren funktionieren für `char`-Werte. Zum Beispiel produziert das folgende Codestücke die Ausgabe `abcdefghijklmnopqrstuvwxyz`:

[source,java]
----
for (char c = 'a'; c <= 'z'; c++) {
    System.out.print(c);
}
----

Daraus lässt sich schliessen, dass die Buchstaben `'a'` bis `'z'` in der Zeichencodierung direkt «hintereinander» stehen. Der Grund, warum die Werte in der Variable `c` hier als Buchstaben und nicht (wie vorher) als Zahl ausgegeben werden, ist dass der Typ der Variable `c` als `char` (und nicht als `int`) definiert wurde.

Der Name der von Java verwendeten Zeichenkodierung lautet _Unicode_. Diese enthält über 144'000 Zeichen aus 159 Schriftsystemen, inklusive Griechisch, Arabisch, Chinesisch, usw. Im Anhang ist die bekannte _ASCII_-Tabelle aufgeführt, welche einer kleinen Teilmenge von Unicode entspricht.


== Konsolen-Eingabe und die `Scanner`-Klasse

Die bisherigen Programme verhalten sich bei jeder Ausführung genau gleich; nur wenn der Code geändert wird, z. B. durch das Ändern einer Variablen-Initialisierung, ergibt sich auch ein neuer Programmablauf. Programme sind aber nützlicher, wenn sie mit Daten arbeiten, _welche die Benutzerin eingibt_.

Eine Art von Eingabe ist die _Konsolen-Eingabe_. Das sind Antworten, die der Benutzer in die Konsole eintippt, wenn das Programm auf Eingabe wartet. Im Prinzip braucht es dazu das Gegenstück zu `System.out.println`; allerdings benötigen wir zusätzlich ein neues Objekt, vom Typ `Scanner`.

=== Die `Scanner`-Klasse

Während man String-Objekte einfach mittels `""` erstellen kann, werden andere Typen von Objekten auf eine andere Art erzeugt. Man braucht dazu einen «Bauplan», der angibt, welche «Form» das Objekt hat. In Java verwendet man als Baupläne _Klassen_. Im Prinzip sind auch die Programme, die wir bisher geschrieben haben (die ja mit `public class` beginnen), Baupläne für Objekte. Dazu kommen wir aber erst in der zweiten Semesterhälfte; vorerst erstellen wir nur Objekte von vordefinierten Klassen.

Um ein Objekt einer bestimmten Klasse zu erstellen, braucht es das Schlüsselwort `new`, den Namen der Klasse, z. B. `Scanner`, und in Klammern gegebenenfalls weitere Informationen:

[source,java]
----
Scanner s = new Scanner(System.in);
----

Dieses `Scanner`-Objekt liest von der Konsole (`System.in`) und wird in der Variable `s` gespeichert. Über diese Variable kann man den Scanner danach beliebig oft verwenden:

[source,java]
----
System.out.print("Gib ein Wort ein: ")
String wort = s.next(); // lies nächstes "Wort" der Eingabe
System.out.println("Dein Wort enthält " + wort.length() + " Zeichen.");
----

Bevor man eine Eingabe einliest, sollte man der Benutzerin zuerst klarmachen, dass sie jetzt etwas eintippen kann; deshalb zuerst der `println`-Befehl oben. Wenn dieser Code ausgeführt wird, sieht das so aus (der fette Text ist die Eingabe):

[subs="+quotes"]
----
Gib ein Wort ein: *programmieren*
Dein Wort enthält 13 Zeichen.
----

Die wichtigsten `Scanner`-Methoden lauten:

[%autowidth]
|===
|Methode        |Beschreibung

|`next()`       |liest das nächste _Token_ (siehe unten) und gibt es als `String` zurück
|`nextDouble()` |liest das nächste Token und gibt es als `double` zurück
|`nextInt()`    |liest das nächste Token und gibt es als `int` zurück
|`nextLine()`   |liest die ganze nächste Zeile und gibt sie als `String` zurück
|===

Wenn ein Programm eine dieser Methoden aufruft, wird die Ausführung des Programms pausiert. Das Programm wartet dann, bis der Benutzer _die nächste ganze Zeile_ eintippt (und mit _Enter_ bestätigt). Auch wenn also nur ein Wort (Token) gelesen werden soll, macht das Programm erst weiter, wenn die Benutzerin das nächste Mal _Enter_ drückt.

=== Tokens und Whitespace

Während `nextLine()` eine Eingabezeile als Ganzes zurückgibt, teilen die anderen `next...`-Methoden die eingegebene Zeile in sogenannte _Tokens_ auf und geben diese einzeln zurück. Ein Token ist ein Teil der Eingabe, der durch bestimmte Trennzeichen vom Rest der Eingabe abgegrenzt ist. Der Scanner verwendet als Trennzeichen standardmässig alle Arten von _Whitespace_, d. h. Leerzeichen, Tabs und Zeilenumbrüche.

Das bedeutet, dass der Scanner beim Einlesen von längeren Texten oft einzelne Wörter als Tokens zurückgibt; schliesslich werden zwischen Wörtern ja auch Leerzeichen geschrieben. Allerdings erkennt der Scanner keine Satzzeichen, o. ä. Zum Beispiel wird der Text:
----
Hallo Java,   "was" läuft?
  "mehr Text" alles-in-1
----
in folgende Tokens aufgeteilt: `Hallo` `Java,` `"was"` `läuft?` `"mehr` `Text"` `alles-in-1`.



[appendix]
== ASCII-Tabelle

Für die Darstellung von Zeichen im Computer wurde der so genannte
ASCII-Code entwickelt. ASCII steht für [.underline]##A##merican
[.underline]##S##tandard [.underline]##C##ode for
[.underline]##I##nformation [.underline]##I##nterchange, was übersetzt
so viel heisst wie Amerikanische Standardcodierung für den
Datenaustausch. Mit Hilfe des 7-Bit ASCII-Codes können 128 Zeichen
(2^7^) dargestellt werden oder umgekehrt wird jedem Zeichen ein
Bitmuster aus 7 Bit zugeordnet. Die Zeichen entsprechen weitgehend der
einer Computertastatur. Der ASCII-Code wurde später auf 8 Bit erweitert,
was die Darstellung von 256 Zeichen (2^8^) erlaubt.

.ASCII Table aus https://commons.wikimedia.org/wiki/File:ASCII-Table-wide.svg
[link=https://commons.wikimedia.org/wiki/File:ASCII-Table-wide.svg]
image::res/ASCII-Table-wide.svg.png[]

[appendix]
== Formattierung von Gleitkommazahlen mit `System.out.printf()`

Die Formatierung von Gleitkommazahlen ist für die Übersichtlichkeit der Ausgabe eines Programms recht wichtig, da standardmässig sehr viele Nachkommastellen angezeigt werden. Zahlen können mit dem Befehl `System.out.printf(...)` formatiert ausgegeben werden.

Statt mehrere Werte mit `+` zusammenzuhängen, wird bei diesem Befehl alles in _einem_ String angegeben. Die Positionen, an denen eine Zahl eingefügt werden, werden bei Gleitkommazahlen mit `%f` markiert. Im Anschluss an den String werden nach einem Komma die Werte angegeben, welche bei den definierten Positionen eingefügt werden sollen.

*Einfaches Beispiel:*
[source,java]
----
double d = 1234.5678;
double d2 = 9.1234;
System.out.printf("d = %f \n", d);  // d= 1234.567800
System.out.printf("d = %f, d2 = %f", d, d2); //d= 1234.567800, d2= 9.123400
----

So sind aber noch keine Vorteile ersichtlich. Um eine Zahl auf eine bestimmte Länge zu kürzen, braucht es zusätzliche «Einstellungen» zwischen dem `%` und den `f`. Hier kann eingefügt werden wie viele Zeichen total ausgegeben werden sollen und/oder wie viele Nachkommastellen.

*Formatierung mit printf():*
[source,java]
System.out.printf( "|%f| |%f|\n" ,          d, -d);
        // |1234,567800| |-1234,567800|
System.out.printf( "|%.2f| |%.2f|\n" ,      d, -d);  // .2 -> zwei Nachkommastellen
        // |1234,57| |-1234,57|
System.out.printf( "|%10f| |%10f|\n" ,      d, -d);  // 10 -> 10 Zahlen
        // |1234,567800| |-1234,567800|

Für mehr Informationen siehe:

* https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/io/PrintStream.html#printf(java.lang.String,java.lang.Object...)
* https://www.baeldung.com/java-printstream-printf